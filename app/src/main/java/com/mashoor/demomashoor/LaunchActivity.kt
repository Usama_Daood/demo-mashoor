package com.mashoor.demomashoor

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mashoor.demomashoor.screen.MainActivity
import kotlinx.android.synthetic.main.activity_launch.*

class LaunchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)

        button_startNow.setOnClickListener({ v ->
            // Open Activity of Story Viwer
            val intent = Intent(this, MainActivity::class.java)

            // start your next activity
            startActivity(intent)
        })



    }
}
