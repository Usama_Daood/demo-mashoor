package com.mashoor.demomashoor.data

import android.os.Parcelable
import com.mashoor.demomashoor.data.Story
import kotlinx.android.parcel.Parcelize

@Parcelize
data class StoryUser(val username: String, val profilePicUrl: String, val stories: ArrayList<Story>) : Parcelable