package com.mashoor.demomashoor.screen

interface PageViewOperator {
    fun backPageView()
    fun nextPageView()
}